const cartBtn = document.querySelector('.cart-btn');
const closeCartBtn = document.querySelector('.close-cart');
const clearCartBtn = document.querySelector('.clear-cart');
const cartDOM = document.querySelector('.cart');
const cartOverlay = document.querySelector('.cart-overlay');
const cartItems = document.querySelector('.cart-items');
const cartTotal = document.querySelector('.cart-total');
const cartContent = document.querySelector('.cart-content');
const productsDOM = document.querySelector('.products-center');
const checkOut =  document.querySelector('.checkout-cart');
const summary = document.querySelector('.item-con');
// const summary2 = document.getElementById('summary2');
const amountModal = document.getElementById("amount");
const closeModal = document.getElementById("close-modal");
const placeOrder = document.getElementById("place-order");
// const checkOutContent = document.querySelector("checkout-content");

let cart = [];
let buttonsDOM = [];

class Products{
  async getProducts(){
    try {
      let result =  await fetch('https://api.jsonbin.io/b/602fa5dabd6b755d0199aeb0/1');
      let data = await result.json();
      // return data;
      let products = data.items;
      products = products.map(item =>{
        const {title,price} = item.fields;
        const {id} = item.sys;
        const image = item.fields.image.fields.file.url;
        return {title,price,id,image}
      })
      return products
    } catch (e) {
      console.log(e);
    }

  }
}

class UI{
  displayProducts(products){
    // console.log(products)
    let result = '';
    products.forEach(product => {
      result +=`
      <article class="product">
        <div class="img-container">
          <img
            src=${product.image}
            alt="product"
            class="product-img"
          />
          <button class="bag-btn" data-id=${product.id}>
            <i class="fas fa-shopping-cart"></i>
            Add to Cart
          </button>
        </div>
        <h3>${product.title}</h3>
        <h4>PHP ${product.price}</h4>
      </article>
      `
    });
    productsDOM.innerHTML = result;
  }
  getBagButtons(){
    const buttons = [...document.querySelectorAll('.bag-btn')];
    // console.log(buttons)
    buttonsDOM = buttons;
    buttons.forEach(button =>{
      let id = button.dataset.id;
      let inCart = cart.find(item => item.id === id);

      if(inCart){
        button.innerText = "In Cart";
        button.disabled = true;
      }
      button.addEventListener('click', (event)=>{
        // console.log(event)
        event.target.innerText = "In Cart"
        event.target.disabled = true;
        //get product from products

        let cartItem = {...Storage.getProduct(id), amount: 1};
        // console.log(cartItem);

        cart = [...cart, cartItem];
        // console.log(cart);

        Storage.saveCart(cart);

        this.setCartValues(cart);

        this.addCartItems(cartItem);

        this.showCart();
      })

    })
  }

  setCartValues(cart){
    let tempTotal = 0;
    let itemsTotal = 0;

    cart.map(item=>{
      tempTotal += item.price * item.amount;
      itemsTotal += item.amount;
    })
    cartTotal.innerText = parseFloat(tempTotal.toFixed(2));
    cartItems.innerText = itemsTotal;
    amountModal.innerText = "Total Amount: " + parseFloat(tempTotal.toFixed(2));
    // console.log(cartTotal,cartItems);
  }
  addCartItems(item){
    const div = document.createElement('div');
    div.classList.add('cart-item');
    div.innerHTML = `
        <img src=${item.image} alt="product" />
        <div>
          <h4>${item.title}/h4>
          <h5>${item.price}</h5>
          <span class="remove-item" data-id=${item.id}>remove</span>
        </div>
        <div>
          <i class="fas fa-chevron-up" data-id=${item.id}></i>
          <p class="item-amount">${item.amount}</p>
          <i class="fas fa-chevron-down" data-id=${item.id}></i>
        </div>
    `;
    // const div2 = document.createElement('div');
    // div2.classList.add('checkout-content');
    //
    // div2.innerHTML = `
    //    <h6>${item.iamge}</h6>
    // `
    // checkOutContent.appendChild(div2);
    cartContent.appendChild(div);

  }

  // addCheckOut(item){
  //   const div2 = document.createElement('div');
  //   div2.classList.add('check-out-item');
  //
  //   div2.innerHTML = `
  //     <h6>${item.title}</h6>
  //   `;
  //
  //   checkOutContent.appendChild(div2);
  // }

  getCheckOut(){
    const item = JSON.parse(localStorage.getItem('cart'));

    console.log(item.title);
  }
  // checkOutItems(item){
  //   const div = document.createElement('div');
  //   div.classList.add('cart-item');
  //   div.innerHTML = `
  //       <img src=${item.image} alt="product" />
  //       <div>
  //         <h4>${item.title}/h4>
  //         <h5>${item.price}</h5>
  //         <span class="remove-item" data-id=${item.id}>remove</span>
  //       </div>
  //       <div>
  //         <i class="fas fa-chevron-up" data-id=${item.id}></i>
  //         <p class="item-amount">${item.amount}</p>
  //         <i class="fas fa-chevron-down" data-id=${item.id}></i>
  //       </div>
  //   `;
  //   checkOut.appendChild(div);
  //   console.log(checkOut);
  // }


  showCart(){
    cartOverlay.classList.add('transparentBcg');
    cartDOM.classList.add('showCart');
  }

  // checkOut(){
  //   this.clearCart();
  // }

  setupApp(){
    cart = Storage.getCart();
    this.setCartValues(cart);
    this.populateCart(cart);
    cartBtn.addEventListener('click', this.showCart);
    closeCartBtn.addEventListener('click', this.hideCart);
    // checkOut.addEventListener('click',this.checkOut);
  }

  populateCart(cart){
    cart.forEach(item=>this.addCartItems(item));

  }

  hideCart(){
    cartOverlay.classList.remove('transparentBcg');
    cartDOM.classList.remove('showCart');
  }


  cartLogic(){

    const itemCheckout = document.getElementById("itemCheckout");
    const qtyCheckOut = document.getElementById("qty");
    const priceCheckOut = document.getElementById("price");

    clearCartBtn.addEventListener('click',()=>{
      this.clearCart();
    });

    closeModal.addEventListener('click',()=>{
      itemCheckout.innerHTML = '';
      qtyCheckOut.innerHTML = '';
      priceCheckOut.innerHTML = '';
    });

    placeOrder.addEventListener('click',()=>{
      let storage = localStorage.getItem('cart')
      let storageParse =  JSON.parse(storage);

      const name = document.getElementById("name").value;
      const email = document.getElementById("email").value;
      const shipping = document.getElementById("shipping").value;


      if (storageParse <= 0) {
        if (name == "" || email == "" || shipping =="") {
          alert("Empty Fields!")
        }else {
          alert("Your Cart is Empty");
        }
      }else {
        if (name == "" || email == "" || shipping == "") {
          alert("Empty Fields!")
        }else {
          alert("Thank you for your Purchase!");
          this.clearCart();
          window.location.reload();
        }
      }



    });

    checkOut.addEventListener('click',()=>{
      this.hideCart();
      const storage = localStorage.getItem('cart')
      const item = JSON.parse(storage);

      item.forEach(function(element)
      {
        itemCheckout.innerHTML += element.title + '</br>';
        qtyCheckOut.innerHTML += element.amount + '</br>';
        priceCheckOut.innerHTML += element.price + '</br>';
      });


      $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
      })


    });

    cartContent.addEventListener('click',event=>{
      // console.log(event.target);
      if (event.target.classList.contains("remove-item")) {
        let removeItem = event.target;
        // console.log(removeItem);
        let id = removeItem.dataset.id;
        cartContent.removeChild(removeItem.parentElement.parentElement);
        this.removeItem(id);
      }else if (event.target.classList.contains("fa-chevron-up")) {
        let addAmount = event.target;
        let id = addAmount.dataset.id;
        let tempItem = cart.find(item=> item.id === id);
        tempItem.amount = tempItem.amount + 1;
        Storage.saveCart(cart);
        this.setCartValues(cart);
        addAmount.nextElementSibling.innerText = tempItem.amount;
      }else if (event.target.classList.contains("fa-chevron-down")){
        let lowerAmount = event.target;
        let id = lowerAmount.dataset.id;
        let tempItem = cart.find(item=> item.id === id);
        tempItem.amount = tempItem.amount - 1;

        if (tempItem.amount > 0) {
          Storage.saveCart(cart);
          this.setCartValues(cart);
          lowerAmount.previousElementSibling.innerText = tempItem.amount;
        }else {
          cartContent.removeChild(lowerAmount.parentElement.parentElement);
          this.removeItem(id);
        }
      }
    });
  }

  clearCart(){
    let cartItems = cart.map(item => item.id);
    cartItems.forEach(id => this.removeItem(id));

    // console.log(cartContent.children);
    while (cartContent.children.length > 0) {
        cartContent.removeChild(cartContent.children[0]);
    }
    this.hideCart();

    const itemCheckout = document.getElementById("itemCheckout");
    const qtyCheckOut = document.getElementById("qty");
    const priceCheckOut = document.getElementById("price");

  }

  removeItem(id){
    cart = cart.filter(item => item.id !== id);
    this.setCartValues(cart);

    Storage.saveCart(cart);
    let button = this.getSingleButton(id);

    button.disabled = false;
    button.innerHTML = `<i class="fas fa-shopping-cart"></i>Add To Cart`;
  }

  getSingleButton(id){
    return buttonsDOM.find(button => button.dataset.id === id);
  }
}

class Storage{
  static saveProducts(products){
    localStorage.setItem("products", JSON.stringify(products));
  }

  static getProduct(id){
    let products = JSON.parse(localStorage.getItem('products'));
    return products.find(product => product.id === id);
  }

  static saveCart(cart){
    localStorage.setItem('cart', JSON.stringify(cart));
  }

  static getCart(){
    return localStorage.getItem('cart')?JSON.parse(localStorage.getItem('cart')):[];
  }
}

document.addEventListener("DOMContentLoaded", ()=>{
  const ui = new UI();
  const products =  new Products();

  ui.setupApp();
  products.getProducts().then(products =>{
    ui.displayProducts(products);
    Storage.saveProducts(products);
  }).then(()=>{
    ui.getBagButtons();
    ui.cartLogic();
  });
});




